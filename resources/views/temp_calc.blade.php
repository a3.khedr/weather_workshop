<!DOCTYPE html>
<html>
    <head>
        <title>Workshop</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
         <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    </head>
    <body>
        <div>
        <h1>Select City and Future Date (Within Five Days) and get Temperature</h1>
        <hr/>
        <div align="center">
            <form>
                <br/>Select City :
                <select name="city" id="city" required>
                </select>
                <br/>
                
                <br/>Select Date :
                <input name = "date" id="date" type="date" min = "" max = "" required>
                
                <br/> <br/>
                <input type="submit" value = "Get Temperature">
                <input id = "reset" type="reset">

            </form>

            </br> </br>


             <table border = "5" width = "100%">
                 <thead>
                    <tr>
                       <th colspan = "4"><H3>Results</H3></th>
                    </tr>
                    <tr>
                       <th>Date</th>
                       <th>Temp</th>
                       <th>Temp_min</th>
                       <th>Temp_max</th>
                    </tr>
                 </thead>
                 <?php
                 echo config('weatherApi.api_name');
                 ?>
                 <!-- @lang('error_messages.unKnown') -->
                 
                 <tbody id = "tbody">

                 </tbody>
                 
              </table>
        <div>
</div>
    </body>
</html>



<!-- This script not working when put it inside '/js/app.js' file -->
        <script> 
            $("#reset").on('click', function() {
                $("tbody").empty();
            });
            $(function () {

                $('form').on('submit', function (e) {

                  e.preventDefault();
                  $.ajax({
                    type: 'get',
                    url: '/temperature',
                    data: $('form').serialize(),
                    dataType: "json",
                    success: function (output) {
                        // alert(output);
                        $("tbody").empty();
                        if(output[0]["cod"] != "200") {
                            alert("Status Code: " + output[0]["cod"] + " " + output[0]["response"]);
                        }
                        else {
                            $("tbody").append('<tr><td>' + output[0]["response"]["date"] +
                                '</td><td>' + output[0]["response"]["temp"] + 
                                '</td><td>' + output[0]["response"]["temp_min"] +
                                '</td><td>' + output[0]["response"]["temp_max"] +
                                '</td></tr>');
                        }
                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown) { 
                        alert("Error: " + errorThrown  + " " + XMLHttpRequest.responseText ); 
                    }
                  });
                });
            });
        </script>
        <script src="/js/app.js" type="text/javascript"></script>