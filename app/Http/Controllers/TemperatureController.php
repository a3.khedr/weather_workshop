<?php

namespace App\Http\Controllers;
use App\library\TemperatureApiStrategyFactory;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redis;
use Barryvdh\Debugbar\Facade;
use Illuminate\Support\Facades\Cache;
use DateTime;

class TemperatureController extends Controller
{
    
    public function get_temp() {
        
        $city_name = substr(json_encode(filter_input(INPUT_GET, 'city', FILTER_SANITIZE_STRING)), 1, -1);
        $date = substr(json_encode(filter_input(INPUT_GET, 'date', FILTER_SANITIZE_STRING)), 1, -1);

        // validate date
        if (! $this->validate_date($date)) {
            return Response(json_encode(['msg' => config('error_messages.notValidDate')]), 403);
        }
        
        // check for request in cache and retrieve it, otherwise, call the library.
        $key = $city_name . " " . $date . " " . config('weatherApi.api_name');

        if(Cache::has($key)) {
            return Cache::get($key);
        }

        $temperatureApiStrategyFactory = new TemperatureApiStrategyFactory();
        $temperatureApiStrategy = $temperatureApiStrategyFactory->
                                                            getTemperatureApiStrategy(config('weatherApi.api_name'));
        $temperature = $temperatureApiStrategy->findTemperature($city_name, $date);

        // save request result in cache
        Cache::put($key, $temperature, 10);
        
        return $temperature;
    }

    private function validate_date($date) {
        return (DateTime::createFromFormat('Y-m-d', $date) !== FALSE);
    }

    
}
