<?php

namespace App\library;
require __DIR__ . '/../../vendor/autoload.php';
use GuzzleHttp;
use GuzzleHttp\Promise;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;
use App\library\openWeatherMapApi as OpenWeatherMapApi;
use App\library\apixuApi as ApixuApi;

class multipleSourceApis extends TemperatureApiStrategy {
	
	
	// find temperature for city = $city_name and date = $date
	// $countryCode is EG(Egyptian cities) as a default value.
	public function findTemperature($city_name, $date, $countryCode = "EG") {
		$temperatureApiStrategyFactory = new TemperatureApiStrategyFactory();
		$temperatureApi1 = $temperatureApiStrategyFactory->
	                                    getTemperatureApiStrategy(config('weatherApi.multipleSource1'));
	    $temperatureApi2 = $temperatureApiStrategyFactory->
	                                    getTemperatureApiStrategy(config('weatherApi.multipleSource2'));

		$full_url1 = $temperatureApi1->get_full_url($city_name);
		$full_url2 = $temperatureApi2->get_full_url($city_name);

		$response = $this->GuzzleGetAsync($full_url1, $full_url2, $date);

		return $response;
	}



	private function get_average($response1, $response2) {
		$arr1 = (array)json_decode($response1);
		$arr2 = (array)json_decode($response2);
		$date = $arr1[0]->response->date;
		$temp = ($arr1[0]->response->temp + $arr2[0]->response->temp)/2;
		$temp_min = ($arr1[0]->response->temp_min + $arr2[0]->response->temp_min)/2;
		$temp_max = ($arr1[0]->response->temp_max + $arr2[0]->response->temp_max)/2;
	
		$obj = array(
			        "date" => $date,
			        "temp" => $temp,
			        "temp_min" => $temp_min,
			        "temp_max" => $temp_max,
			    );
		$res = array(["cod" => "200", "response" => $obj]);
		return json_encode($res);

	}


	private function GuzzleGetAsync($full_url1, $full_url2, $date) {

		$temperatureApiStrategyFactory = new TemperatureApiStrategyFactory();

		$client = new Client();
		$promises = [];
		$promises[] = $client->requestAsync('GET', $full_url1);
		$promises[] = $client->requestAsync('GET', $full_url2);
		$results = Promise\settle($promises)->wait();

		$i = 0;
		$response1 = ""; $response2 = "";
		foreach($results as $domain => $result)
		{

		    if($result['state'] === 'fulfilled')
		    {
		        $response = $result['value'];
		        if($response->getStatusCode() == 200)
		        {
		        	// echo @lang('error_messages.unknown');
		        	// echo __('error_messages.unKnown');

       				 $temperatureApi1 = $temperatureApiStrategyFactory->
                                                      getTemperatureApiStrategy(config('weatherApi.multipleSource1'));
                     $temperatureApi2 = $temperatureApiStrategyFactory->
                                                      getTemperatureApiStrategy(config('weatherApi.multipleSource2'));

		            if($i == 0){
		           		$response1 = $temperatureApi1 -> 
		           						parse_response((array) json_decode((string)$response->getBody(), true), $date);
		            } 
		           	else
		           	{
		           		$response2 = $temperatureApi2 -> 
		           						parse_response((array) json_decode((string)$response->getBody(), true), $date);
		           	}
		        }
		        else{
		            echo $response->getStatusCode();
		        }
		    }
		    else if($result['state'] === 'rejected'){ // notice that if call fails guzzle returns is as state rejected with a reason.

		        echo ('ERR: ' . $result['reason'] );
		    }
		    else{
		        echo (config('error_messages.unKnown'));
		    }
		    $i++;
		}
		return $this -> get_average($response1, $response2);
	}

	
}