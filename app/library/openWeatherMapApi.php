<?php

namespace App\library;
require __DIR__ . '/../../vendor/autoload.php';
use GuzzleHttp;
use GuzzleHttp\Promise;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;

class openWeatherMapApi extends TemperatureApiStrategy {
	#################################################
    # -> NOTES: 
    # 1- Open Weather Map Api supports finding temperatures for only next five days for a free account.  
    # 2- There is no API end point that returns temperature of city and date on OWM Api.
    # 3- This code gets temperature for a city = $city_name for the next five days and returns empty array for invalid date.
    #################################################

	// find temperature for city = $city_name and date = $date
	// $countryCode is EG(Egyptian cities) as a default value.
	public function findTemperature($city_name, $date, $countryCode = "EG") {

		$full_url = $this->get_full_url($city_name);

		$response = $this->GuzzleGet($full_url, $date);

		return $response;
	}



	public function get_full_url($city_name, $countryCode = "EG") {
		$base_uri = "http://api.openweathermap.org/data/2.5/forecast?";
		$appid = env("OWM_API_ID", "9c37315d78dbc4d3089c619d142fd52f");
		$query = $city_name. ",".$countryCode;

		$full_url = $base_uri. "appid=". $appid. "&q=". $query;

		return $full_url;
	}

	


	// private method to parse json response from OWM Api to get temperature on date = $date
	 public function parse_response($json_response, $date) {

	 	$obj = "";
		for( $i  = 0; $i < sizeof($json_response["list"]); $i++ ) {
			$date_txt = $json_response["list"][$i]["dt_txt"];

			if(strpos($date_txt, $date) !== false) {

				$obj = array(
			        "date" => $date,
			        "temp" => $json_response["list"][$i]["main"]["temp"],
			        "temp_min" => $json_response["list"][$i]["main"]["temp_min"],
			        "temp_max" => $json_response["list"][$i]["main"]["temp_max"],
			    );
			}
		}
		$res = array(["cod" => "200", "response" => $obj]);
		return json_encode($res);
	}

	
}