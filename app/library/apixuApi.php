<?php

namespace App\library;
require __DIR__ . '/../../vendor/autoload.php';
use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;

class apixuApi extends TemperatureApiStrategy {
	

	// find temperature for city = $city_name and date = $date
	// $countryCode is EG(Egyptian cities) as a default value.
	public function findTemperature($city_name, $date, $countryCode = "EG") {
		$full_url = $this->get_full_url($city_name);

		$response = $this->GuzzleGet($full_url, $date);

		return $response;
	}

	
	public function get_full_url($city_name, $countryCode = "EG") {
		$base_uri = "http://api.apixu.com/v1/forecast.json?";
		$key = env("APIXU_KEY", "11e16fc83ce8459db6a122223180507");
		$query = $city_name. ",".$countryCode;

		$full_url = $base_uri. "key=". $key. "&q=". $query . "&days=5";

		return $full_url;
	}


	// private method to parse json response from OWM Api to get temperature on date = $date
	 public function parse_response($json_response, $date) {

	 	$obj = "";
		for( $i  = 0; $i < sizeof($json_response["forecast"]["forecastday"]); $i++ ) {
			$date_txt = $json_response["forecast"]["forecastday"][$i]["date"];
			if(strpos($date_txt, $date) !== false) {
				$obj = array(
			        "date" => $json_response["forecast"]["forecastday"][$i]["date"],
			        "temp" => $json_response["forecast"]["forecastday"][$i]["day"]["avgtemp_c"] + 273.15, // to kelvin
			        "temp_min" => $json_response["forecast"]["forecastday"][$i]["day"]["mintemp_c"] + 273.15,// to kelvin
			        "temp_max" => $json_response["forecast"]["forecastday"][$i]["day"]["maxtemp_c"] + 273.15,// to kelvin
			    );
			}
		}
		$res = array(["cod" => "200", "response" => $obj]);
		return json_encode($res);
	}

	
}