<?php

namespace App\library;
require __DIR__ . '/../../vendor/autoload.php';
use GuzzleHttp;
use GuzzleHttp\Promise;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;
use App\library\openWeatherMapApi as OpenWeatherMapApi;
use App\library\apixuApi as ApixuApi;

abstract class TemperatureApiStrategy {

	public abstract function findTemperature($city_name, $date);

	public function GuzzleGet($full_url, $date) {
		try {

			$client = new Client();
		    $res = $client->request('GET', $full_url, ['http_errors' => false]);

		    if($res->getStatusCode() == 200) {
	            $json_response =  json_decode($res->getBody(), TRUE);
	            $res  = $this->parse_response($json_response, $date);
	            return $res;
	        }
	        else {
	        	$response = json_decode($res->getBody());
	        }
		} catch (RequestException $e) {
			$response = "Network Error";
			if ($e->hasResponse()) {
		        $response = Psr7\str($e->getResponse());
		    }
		}

		$res = array(["cod" => "400", "response" => $response]);
		return json_encode($res);
	}


}