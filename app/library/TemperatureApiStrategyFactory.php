<?php

namespace App\library;

class TemperatureApiStrategyFactory {

	public function getTemperatureApiStrategy($strategy_type) {
		switch($strategy_type) {
			case "owm" :
				return new openWeatherMapApi;
			case "apixu" :
				return new apixuApi;
			case "multipleSource" :
				return new multipleSourceApis;
			default:
				return new notValidApi;
		}
	}
}