<?php

/*
    |--------------------------------------------------------------------------
    | API name 
    |--------------------------------------------------------------------------
    |
    | This value determines the "api" that is used to get the weather.
    | api_name can be one of the following values:
    | owm -> open weather map API
    | apixu -> apixu API
    | multipleSource -> multiple Source API
    | 
    */

return [
	'api_name' => 'multipleSource',
	'multipleSource1' => 'apixu',
	'multipleSource2' => 'owm',
];