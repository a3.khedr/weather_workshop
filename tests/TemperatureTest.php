<?php


use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;


use Illuminate\Http\Response;
use PHPUnit\Framework\TestCase;
use App\Http\Controllers;
use App\library\TemperatureApiStrategyFactory;




class TemperatureTest extends TestCase
{

     #################################################
    # -> NOTES: 
    # 1- I tried testing get request to '/temperature', 
    #    Unfortunately, methods get() and call() raise some errors, canNOT identify those methods,
    #    So i tested the code inside tha API endpoint.
    #    And tested API calls through postman and attached link for documentation at postmanDocumentation.txt .  
    #################################################
 

    public function testObjectReturnedFromFactory() {
        $temperatureApiStrategyFactory = new TemperatureApiStrategyFactory();
        $temperatureApiStrategy = $temperatureApiStrategyFactory->getTemperatureApiStrategy("owm");
        $url = $temperatureApiStrategy->get_full_url("Zifta");
        $this->assertContains('api.openweathermap.org' ,$url);


        $temperatureApiStrategy = $temperatureApiStrategyFactory->getTemperatureApiStrategy("apixu");
        $url = $temperatureApiStrategy->get_full_url("Zifta");
        $this->assertContains('api.apixu.com' ,$url);
    }


    // testing getting temperature for successful city and date
    public function testGetTemperatureSuccessfully() {
        $city_name = "Zifta";
        $date = new DateTime();
        $date = $date->format("Y-m-d");
        $temperatureApiStrategyFactory = new TemperatureApiStrategyFactory();
        $temperatureApiStrategy = $temperatureApiStrategyFactory->getTemperatureApiStrategy("owm");
        $temperature = $temperatureApiStrategy->findTemperature($city_name, $date);

        $this->assertContains('"cod":"200"' ,$temperature);

        // test response structure
        $this->assertContains('temp' ,$temperature);
        $this->assertContains('temp_min' ,$temperature);
        $this->assertContains('temp_max' ,$temperature);

    }


    // testing getting temperature for city not found
    public function testGetTemperatureWrongCityName() {
        $city_name = "Ziftaaa";
        $date = date("Y/m/d");
        $temperatureApiStrategyFactory = new TemperatureApiStrategyFactory();
        $temperatureApiStrategy = $temperatureApiStrategyFactory->getTemperatureApiStrategy("owm");
        $temperature = $temperatureApiStrategy->findTemperature($city_name, $date);

        $this->assertContains('"cod":"404"' ,$temperature);
    }


    // testing getting temperature for valid city but Invalid date(invalid format, old or future dates)
    public function testGetTemperatureInvalidDate() {
        $city_name = "Zifta";
        // invalid date format
        $date = "2018-06-188";
        $temperatureApiStrategyFactory = new TemperatureApiStrategyFactory();
        $temperatureApiStrategy = $temperatureApiStrategyFactory->getTemperatureApiStrategy("owm");
        $temperature = $temperatureApiStrategy->findTemperature($city_name, $date);

        $this->assertContains('"response":""' ,$temperature);

        // old date
        $date = new DateTime();
        $date->modify("-7 day");
        $date = $date->format("Y-m-d");

        $temperature = $temperatureApiStrategy->findTemperature($city_name, $date);

        $this->assertContains('"response":""' ,$temperature);

        // future date more than five days
        $date = new DateTime();
        $date->modify("+7 day");
        $date = $date->format("Y-m-d");


        $temperature = $temperatureApiStrategy->findTemperature($city_name, $date);

        $this->assertContains('"response":""' ,$temperature);

    }


}
